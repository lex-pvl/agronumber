import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/steps",
    name: "Steps",
    component: () => import("../views/Steps.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue"),
  },
  {
    path: "/recover",
    name: "Recover",
    component: () => import("../views/Recover.vue"),
  },
  {
    path: "/documents",
    name: "Documents",
    component: () => import("../views/Documents.vue"),
  },
  {
    path: "/documents/:id",
    name: "Document",
    component: () => import("../views/Document.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
